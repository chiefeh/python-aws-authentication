# Python AWS Authentication

Python code to login to AWS

Setup awscli on your machine (I'm using Fedora)
```
sudo dnf install awscli -y
```

Configure the awscli, you will need programmatic access to an AWS account.
```
aws configure
```
